# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Film(scrapy.Item):
    nombre = scrapy.Field()
    imagen = scrapy.Field()
    horarios = scrapy.Field()
    dia = scrapy.Field()
    descripcion = scrapy.Field()
    director = scrapy.Field()
    pais = scrapy.Field()
    anio = scrapy.Field()
    duracion = scrapy.Field()
    orden = scrapy.Field()
