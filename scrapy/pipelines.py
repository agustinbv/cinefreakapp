# -*- coding: utf-8 -*-
import logging
from hashlib import md5
from datetime import datetime
from firebase import firebase

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

logger = logging.getLogger(__name__)


class FilmPipeline(object):

    def process_item(self, item, spider):
        for field in ('director', 'pais', 'anio', 'duracion'):
            item[field] = self._parse_info(item[field])
        return item

    def _parse_info(self, item):
        try:
            return item.split(':').pop().strip()
        except:
            logger.error("NO SE PUDO PARSEAR %s" % item)
            return item


class FirebasePipeline(object):

    def __init__(self):
        self.dsn = 'https://cinefreakapp.firebaseio.com/'
        self.token = 'f2eVwBghx4NhRDSfHao3Z6XXc7AxgPDm0TUXFRhS'
        self.email = 'agustinbv@gmail.com'

    def process_item(self, item, spider):
        auth = firebase.FirebaseAuthentication(self.token, self.email, admin=True)
        fb = firebase.FirebaseApplication(self.dsn, authentication=auth)

        # programacion actual
        data = dict(item)
        fb.patch('/programacion/%s' % data['dia'], data)

        # historial de peliculas
        n = data['nombre'].encode('utf-8')
        d = data['director'].encode('utf-8')
        key = md5('%s:%s' % (n,d)).hexdigest()
        fb.patch('/peliculas/%s' % key, {
            'nombre': data['nombre'],
            'director': data['director'],
            'fecha': datetime.now().isoformat()
        })

        return item
