# -*- coding: utf-8 -*-
import scrapy
from cinefreak.items import Film


class ProgramacionSpider(scrapy.Spider):
    name = "programacion"
    allowed_domains = ["cinefreakshow.com.ar"]
    start_urls = (
        'http://www.cinefreakshow.com.ar/seccion.php?id=25',
    )

    def parse(self, response):
        films = response.xpath("//div[@class='content03' and div[@class='pelicula_right']]")
        orden = 0

        for f in films:
            nombre = f.xpath(".//div[@class='title_text']//text()").extract_first()
            horarios = f.xpath(".//div[@class='fecha_hora']/text()").extract()
            imagen = f.xpath(".//div[@class='pelicula_imagen']/img/@src").extract_first()
            descripcion = f.xpath(".//div[@class='data_text']/text()").extract_first()
            director, pais, anio, duracion = f.xpath(".//div[@class='data_text']/strong/text()").extract()
            
            film = Film()
            film['nombre'] = nombre
            film['horarios'] = [h.strip() for h in horarios if h.strip()]
            film['dia'] = film['horarios'][0].split()[0] # boom?
            film['imagen'] = response.urljoin(imagen)
            film['descripcion'] = descripcion.strip()
            film['director'] = director.strip()
            film['pais'] = pais.strip()
            film['anio'] = anio.strip()
            film['duracion'] = duracion.strip()
            
            orden += 1    
            film['orden'] = orden

            yield film
