angular.module('cinefreak.controllers', [])

.controller('FilmsCtrl', function($q, $scope, $timeout, $ionicLoading, Films) {
  
  $ionicLoading.show({
    noBackdrop: true
  });

  //$scope.films = Films.data;

  Films.all()
    .then(function(films) {
      $ionicLoading.hide();
      $scope.films = films;
    }, function(err) {
      console.log(err);
    })
})

.controller('FilmDetailCtrl', function($scope, $stateParams, Films) {
  $scope.film = Films.get($stateParams.filmName);
})

.controller('MapCtrl', function($scope) {
 
  $scope.initMap = function() {
    var myLatlng = new google.maps.LatLng(-34.9140446, -57.9516899);

    var mapOptions = {
        center: myLatlng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    
    var myLocation = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: "Dardo Rocha"
    });

    $scope.map = map;
  }
})