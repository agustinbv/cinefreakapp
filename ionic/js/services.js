angular.module('cinefreak.services', [])

.factory('Films', function($firebaseArray) {

  var ref = new Firebase('https://cinefreakapp.firebaseio.com/programacion').orderByChild('orden')
  var obj = $firebaseArray(ref);

  return {
    all: function() {
      var promise = obj.$loaded()
      
      promise.then(function(data) {
          //console.log(data === obj); // true
        })
        .catch(function(error) {
          console.error("Error:", error);
        })

      return promise;
    },
    remove: function(film) {
      obj.splice(obj.indexOf(film), 1);
    },
    get: function(nombre) {
      for (var i = 0; i < obj.length; i++) {
        if (obj[i].nombre === nombre) {
          return obj[i];
        }
      }
      return null;
    },
    data: obj
  };
})