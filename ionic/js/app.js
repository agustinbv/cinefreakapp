// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('cinefreak', ['ionic','ionic.service.core', 'firebase', 'cinefreak.controllers', 'cinefreak.services'])

//.constant('SCRAPYRT', 'http://localhost:9080/crawl.json?spider_name=programacion&url=http://www.cinefreakshow.com.ar/seccion.php?id=25')

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    var push = new Ionic.Push({
      "debug": true
    });

    push.register(function(token) {
      push.saveToken(token);  // persist the token in the Ionic Platform
    });

    // push.register(function(token) {
    //   console.log("Device token:",token.token);
    // });
    
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
  })

  .state('app.films', {
    url: '/programacion',
    views: {
      'menuContent': {
        templateUrl: 'templates/films.html',
        controller: 'FilmsCtrl'
      }
    }
  })

  .state('app.film', {
    url: '/films/:filmName',
    views: {
      'menuContent': {
        templateUrl: 'templates/film.html',
        controller: 'FilmDetailCtrl'
      }
    }
  })

  .state('app.map', {
    url: '/mapa',
    views: {
      'menuContent': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    }

  })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/programacion');
})

